﻿#include <iostream>
#include <thread>
#include <chrono>
#include <ctime>

void sleep(int milliseconds) {
    std::this_thread::sleep_for(std::chrono::milliseconds(milliseconds));
}

int rand_range(int low, int high) {
    // Error if low is higher than high. That might cause some weirdness.
    if (low >= high) {
        throw std::range_error::range_error("high must be higher than low");
    }

    int mod = high - low;

    return low + (rand() % mod);
}

void PrintIntro() {
    std::cout << "The name's Jond. Bames Jond.\n\n";
    std::cout << "You're a spy tasked with breaking into Professor Badstuffs' computer network\nto find the deactivation codes for his nuclear weapons.\n";
    std::cout << "How did he get nukes? Why is his secure computer network inside a casino?\n";
    std::cout << "You have no idea, but there's no time for questions like that! All that matters\nis stopping Professor Badstuffs.\n";
    std::cout << "Get in there, you go getter, you!\n\n";
    std::cout << "The server's display blinks menacingly at you. I mean, it's just a terminal\nwindow, with a space for a 3 digit code, but it's still super menacing!\n";
    std::cout << "Looks like those hacking lessons your mom paid for in high school are finally\ngoing to pay off.\n\n\n";
}

void PrintSuccess() {
    std::cout << "\nYour nimble fingers take down a layer of the firewall, getting you \none step closer to those sweet, sweet nuke files.\n";
}

void PrintWin() {
    std::cout << "\nThe firewall shuts down, and the data streams to your thumb drive.\nYou make your way back out to the casino floor, and successfully escape with the data!\n";
}

void PrintLose() {
    std::cout << "\nThe alarm has gone off! You need to get out before the guards come...\nMaybe if you spend some time hiding at the baccarat table, you can try again.\n";
}

void PrintSystemPrompt(int LevelDifficulty) {
    sleep(1500);
    std::cout << " _____________________________________________________________\n";
    std::cout << "/                                                             \\\n";
    sleep(75);
    std::cout << "|  ======       ==      ======           ======      ====     |\n";
    sleep(75);
    std::cout << "|  ==   ==     ====     ==   ==         ==    ==   ===  ===   |\n";
    sleep(75);
    std::cout << "|  ======     ==  ==    ==   ==         ==    ==     ===      |\n";
    sleep(75);
    std::cout << "|  ==   ==   ========   ==   ==         ==    ==       ===    |\n";
    sleep(75);
    std::cout << "|  ==   ==   ==    ==   ==   ==         ==    ==   ===  ===   |\n";
    sleep(75);
    std::cout << "|  ======    ==    ==   ======           ======      =====    |\n";
    sleep(75);
    std::cout << "|                                                             |\n";
    sleep(75);
    std::cout << "|                                                v" << LevelDifficulty << ".0.0       |\n";
    sleep(75);
    std::cout << "\\_____________________________________________________________/\n\n";
    sleep(2000);
    std::cout << "Booting system";
    sleep(750);
    std::cout << ".";
    sleep(750);
    std::cout << ".";
    sleep(750);
    std::cout << ".";
    sleep(1000);
    std::cout << "\nLoading login prompt";
    sleep(400);
    std::cout << ".";
    sleep(400);
    std::cout << ".";
    sleep(400);
    std::cout << ".";
    sleep(750);
}

void PrintLoginPrompt() {
    // Print out a fake terminal login prompt
    std::cout << "Username: ";
    sleep(500);
    std::cout << "g";
    sleep(300);
    std::cout << "u";
    sleep(250);
    std::cout << "e";
    sleep(550);
    std::cout << "s";
    sleep(300);
    std::cout << "t";
    sleep(150);
    std::cout << "\nPassword: ";
}

void PrintSpacer() {
    std::cout << "\n\n\n";
}

bool PlayGame(int LevelDifficulty) {
    const int FirstDigit = rand_range(LevelDifficulty, LevelDifficulty * 2 + LevelDifficulty);
    const int SecondDigit = rand_range(LevelDifficulty, LevelDifficulty * 2 + LevelDifficulty);
    const int ThirdDigit = rand_range(LevelDifficulty, LevelDifficulty * 2 + LevelDifficulty);

    const int CodeSum = FirstDigit + SecondDigit + ThirdDigit;
    const int CodeProduct = FirstDigit * SecondDigit * ThirdDigit;

    PrintSystemPrompt(LevelDifficulty);
    std::cout << "\n\n";
    std::cout << "  [+] The sum of the digits of the code is " << CodeSum << std::endl;
    std::cout << "  [+] The product of the digits of the code is " << CodeProduct << std::endl << std::endl;

    sleep(750);

    PrintLoginPrompt();

    int GuessA;
    int GuessB;
    int GuessC;
    std::cin >> GuessA;
    std::cin >> GuessB;
    std::cin >> GuessC;

    int GuessSum = GuessA + GuessB + GuessC;
    int GuessProduct = GuessA * GuessB * GuessC;

    std::cout << "You guessed " << GuessA << GuessB << GuessC;
    std::cout << "\n\n";
    std::cout << "  [+] Your guess' sum was " << GuessSum << std::endl;
    std::cout << "  [+] Your guess' product was " << GuessProduct << std::endl;

    if (CodeSum == GuessSum && CodeProduct == GuessProduct)
    {
        PrintSuccess();
        return true;
    }
    else
    {
        PrintLose();
        return false;
    }
}

int main()
{
    srand(time(NULL));
    int LevelDifficulty = 1;
    const int MaxDifficulty = 5;

    PrintIntro();

    while (LevelDifficulty <= MaxDifficulty)
    {
        bool bLevelComplete = PlayGame(LevelDifficulty);
        std::cin.clear();
        std::cin.ignore();
        if (bLevelComplete)
        {
            ++LevelDifficulty;
        }
        sleep(2000);
        PrintSpacer();
    }

    PrintWin();

    return 0;
}
